<div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
	  <form action="results" method="POST">
		<div class="input-group">
		  <input type="text" class="form-control bg-light border-0 small" name="keyword" placeholder="Search artist..." value="<?php echo $keyword; ?>" aria-label="Search" aria-describedby="basic-addon2">
		  <div class="input-group-append">
			<button class="btn btn-primary" type="submit">
			  <i class="fas fa-search fa-sm"></i>
			</button>
		  </div>
		</div>
	  </form>
	</div>
  </div>
  <br>
  <br>
  <div class="row">
	<div class="col-lg-4 col-md-4 col-sm-12">
	  <?php echo $results_count; ?> Results found for <b>"<?php echo $keyword; ?>"</b>
	</div>
  </div>
  <br>
  <?php
  if ($results_count > 0)
  {
  ?>  
  <div class="row">
    <?php
	  $i=1;
	  foreach ($profiles as $profile)
	  {
	?>
	<div class="col-lg-4 col-md-4 col-sm-12">
	  <div class="card border-left-primary shadow h-100 py-2">
		<div class="card-body">
		  <div class="row no-gutters align-items-center">
			<div class="col-3">
			  <img class="img-profile rounded-circle" width="60" height="60" src="<?php echo $profile['img']; ?>">
			</div>
			<div class="col-auto">
			  <div class="h6 mb-0 font-weight-bold text-gray-800"><a href="<?php echo base_url().'artist/'.$profile['name']; ?>"><?php echo $profile['name']; ?></a></div>
			  <div class="text-xs font-weight-bold text-primary mb-1"><a href="<?php echo $profile['facebook']; ?>"><?php echo str_replace(array('http://www.','https://www.'),array('',''),$profile['facebook']); ?></a></div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<?php
	    if (($i % 3) == 0)
		{
		  echo '</div><br><div class="row">';
		}
		$i++;
	  }
	?>
  </div>
  <?php
  }
  ?>
</div>