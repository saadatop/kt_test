<div class="container">
  <div class="row">
	<div class="col-lg-4 col-md-4 col-sm-12">
	  <a href="javascript:window.history.back();">< Back to results</a>
	</div>
  </div>
  <br>
  <?php
  if ($results_count > 0)
  {
  ?>  
  <div class="row">
    <?php
	  foreach ($profiles as $profile)
	  {
	?>
	<div class="col-lg-4 col-md-4 col-sm-12">
	  <div class="card border-left-primary shadow h-100 py-2">
		<div class="card-body">
		  <div class="row no-gutters align-items-center">
			<div class="col-3">
			  <img class="img-profile rounded-circle" width="60" height="60" src="<?php echo $profile['img']; ?>">
			</div>
			<div class="col-auto">
			  <div class="h6 mb-0 font-weight-bold text-gray-800"><a href="<?php echo base_url().'artist/'.$profile['name']; ?>"><?php echo $profile['name']; ?></a></div>
			  <div class="text-xs font-weight-bold text-primary mb-1"><a href="<?php echo $profile['facebook']; ?>"><?php echo str_replace(array('http://www.','https://www.'),array('',''),$profile['facebook']); ?></a></div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<?php
	  }
	?>
  </div>
  <?php
  }
  ?>
  
  <br>
  <div class="row">
	<div class="col-lg-4 col-md-4 col-sm-12">
	  <?php echo $results_count; ?> upcoming events
	</div>
  </div>
  <br>
  
  <?php
  if ($results_count > 0)
  {
  ?>  
  <div class="row">
    <?php
	  $i=1;
	  foreach ($events as $event)
	  {
	?>
	<div class="col-lg-4 col-md-4 col-sm-12">
	  <div class="card border-left-primary shadow h-100 py-2">
		<div class="card-header">
		  <span class="text-uppercase">EVENT DETAILS</span>
		</div>
		<div class="card-body">
		  <div class="row no-gutters align-items-center">
			<div class="col-6">
			  <div class="row">
			    <div class="col-12 small">Country</div>
			  </div>
			  <div class="row">
			    <div class="col-12">
			      <div class="text-xs font-weight-bold text-primary mb-1"><?php echo $event['country']; ?></div>
			    </div>
			  </div>
			</div>
			<div class="col-6">
			  <div class="row">
			    <div class="col-12 small">City</div>
			  </div>
			  <div class="row">
			    <div class="col-12 text-xs font-weight-bold text-primary mb-1"><?php echo $event['city']; ?></div>
			  </div>
			</div>
		  </div>
		  <div class="row no-gutters align-items-center">
			<div class="col-6">
			  <div class="row">
			    <div class="col-12 small">Venue</div>
			  </div>
			  <div class="row">
			    <div class="col-12 text-xs font-weight-bold text-primary mb-1"><?php echo $event['venue']; ?></div>
			  </div>
			</div>
			<div class="col-6">
			  <div class="row">
			    <span class="col-12 small">Date</span>
			  </div>
			  <div class="row">
			    <div class="col-12 text-xs font-weight-bold text-primary mb-1"><?php echo date('d M, Y', strtotime($event['date'])); ?></div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<?php
	    if (($i % 3) == 0)
		{
		  echo '</div><br><div class="row">';
		}
		$i++;
	  }
	?>
  </div>
  <?php
  }
  ?>
</div>