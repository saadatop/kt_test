  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/lodash.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/vendor/fontawesome-free/js/all.min.js') ?>"></script>
  
  <footer class="sticky-footer bg-white">
	<div class="container my-auto">
	  <div class="copyright text-center my-auto">
		<span>Copyright © 2019</span>
	  </div>
	</div>
  </footer>
</body>
</html>