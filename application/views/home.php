<div class="container">
  <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
	  <form action="search" method="POST">
		<div class="input-group">
		  <input type="text" class="form-control bg-light border-0 small" name="keyword" placeholder="Search artist..." aria-label="Search" aria-describedby="basic-addon2">
		  <div class="input-group-append">
			<button class="btn btn-primary" type="submit">
			  <i class="fas fa-search fa-sm"></i>
			</button>
		  </div>
		</div>
	  </form>
	</div>
  </div>
</div>