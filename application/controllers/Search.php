<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller
{
  /**
  * This is default constructor of the class
  */
  public function __construct()
  {
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
  }

  /**
  * Index Page for this controller.
  */
  public function index()
  {
	$data['keyword'] = $this->input->post('keyword', TRUE);;
	$app_id = '123123';
	$json = file_get_contents("https://rest.bandsintown.com/artists/".$data['keyword']."?app_id=$app_id");
    $result = json_decode($json, true);
	$sn = 0;
	//foreach ($result as $result) {
		$data['profiles'][$sn]['id'] = $result['id'];
		$data['profiles'][$sn]['name'] = $result['name'];
		$data['profiles'][$sn]['facebook'] = $result['facebook_page_url'];
		$data['profiles'][$sn]['img'] = $result['thumb_url'];
		$sn++;
	//}
	$data['results_count'] = count($data['profiles']);
	$this->output->cache(10);
	$this->load->view('header');
    $this->load->view('search', $data);
    $this->load->view('footer');
  }
}
?>