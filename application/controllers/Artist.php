<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Artist extends CI_Controller
{
  /**
  * This is default constructor of the class
  */
  public function __construct()
  {
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
  }

  /**
  * Index Page for this controller.
  */
  public function artist($artistcode)
  {
	$data['keyword'] = $this->uri->segment(2, 0);
	$app_id = '123123';
	$json = file_get_contents("https://rest.bandsintown.com/artists/".$data['keyword']."/events?app_id=$app_id");
    $result = json_decode($json, true);
	$data['profiles'][0]['name'] = $result[0]['artist']['name'];
	$data['profiles'][0]['facebook'] = $result[0]['artist']['facebook_page_url'];
	$data['profiles'][0]['img'] = $result[0]['artist']['thumb_url'];
	$sn = 0;
	foreach ($result as $event) {
	  $data['events'][$sn]['country'] = $event['venue']['country'];
	  $data['events'][$sn]['city'] = $event['venue']['city'];
	  $data['events'][$sn]['venue'] = $event['venue']['name'];
	  $data['events'][$sn]['date'] = $event['datetime'];
	  $sn++;
	}
	$data['results_count'] = count($data['events']);
	$this->output->cache(10);
	$this->load->view('header');
    $this->load->view('artist', $data);
    $this->load->view('footer');
  }
}
?>